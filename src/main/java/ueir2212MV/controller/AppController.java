package ueir2212MV.controller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import ueir2212MV.exception.IntrebareValidatorFailedException;
import ueir2212MV.model.Intrebare;
import ueir2212MV.model.Statistica;
import ueir2212MV.model.Test;
import ueir2212MV.repository.IntrebariRepository;
import ueir2212MV.exception.DuplicateIntrebareException;
import ueir2212MV.exception.NotAbleToCreateStatisticsException;
import ueir2212MV.exception.NotAbleToCreateTestException;
import ueir2212MV.util.IntrebareValidator;

public class AppController {
	
	private IntrebariRepository intrebariRepository;

	public AppController(){
		intrebariRepository = new IntrebariRepository();
	}
	public AppController(String file) {
		intrebariRepository = new IntrebariRepository();
		loadIntrebariFromFile(file);
	}
	
	public Intrebare addNewIntrebare(String enunt, String varianta1, String varianta2, String varianta3, String variantaCorecta, String domeniu) throws DuplicateIntrebareException, IntrebareValidatorFailedException {
		Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2,varianta3, variantaCorecta, domeniu);
		IntrebareValidator.validateEnunt(intrebare.getEnunt());
		IntrebareValidator.validateVarianta1(intrebare.getVarianta1());
		IntrebareValidator.validateVarianta2(intrebare.getVarianta2());
		IntrebareValidator.validateVarianta3(intrebare.getVarianta3());
		IntrebareValidator.validateVariantaCorecta(intrebare.getVariantaCorecta());
		IntrebareValidator.validateDomeniu(intrebare.getDomeniu());
		intrebariRepository.addIntrebare(intrebare);
		
		return intrebare;
	}
	
	public boolean exists(Intrebare intrebare){
		return intrebariRepository.exists(intrebare);
	}
	
	public Test createNewTest() throws NotAbleToCreateTestException {

        List<Intrebare> testIntrebari = new ArrayList<>();
        List<String> domenii = new LinkedList<String>();
        Intrebare intrebare;
        Test test = new Test();

		if (intrebariRepository.getIntrebari().size() < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");

		if (intrebariRepository.getNumberOfDistinctDomains() < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");

		while (testIntrebari.size() < 5){
			intrebare = intrebariRepository.pickRandomIntrebare();

		if (!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())) {
			testIntrebari.add(intrebare);
			domenii.add(intrebare.getDomeniu());
		}
		}

		test.setIntrebari(testIntrebari);
		return test;
		
	}
	public int dim(){
		return intrebariRepository.dim();
	}
	
	public void loadIntrebariFromFile(String file){
		intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile(file));
	}
	
	public Statistica getStatistica() throws NotAbleToCreateStatisticsException{
		
		if(intrebariRepository.getIntrebari().isEmpty())
			throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");
		
		Statistica statistica = new Statistica();
		for(String domeniu : intrebariRepository.getDistinctDomains()){
			int countIntrebari = 0;
			for(Intrebare intrebare : intrebariRepository.getIntrebari()){
				if(intrebare.getDomeniu().equals(domeniu)){
					countIntrebari++;
				}
			}
			addStatistica(statistica,domeniu, countIntrebari);
		}
		
		return statistica;
	}
	public void addStatistica(Statistica s, String key, Integer value){
		s.getIntrebariDomenii().put(key, value);
	}

}
