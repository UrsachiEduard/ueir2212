package ueir2212MV.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import ueir2212MV.exception.DuplicateIntrebareException;
import ueir2212MV.exception.IntrebareValidatorFailedException;
import ueir2212MV.exception.NotAbleToCreateTestException;
import ueir2212MV.model.Intrebare;
import ueir2212MV.model.Statistica;

import ueir2212MV.controller.AppController;
import ueir2212MV.exception.NotAbleToCreateStatisticsException;
import ueir2212MV.model.Test;

//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

    private static final String file = "C:\\Users\\Edi\\Desktop\\An 3 Semestrul 2\\VVSS\\ueir2212\\src\\main\\java\\ueir2212MV\\intrebari.txt";

    public static void main(String[] args) throws IOException, DuplicateIntrebareException, IntrebareValidatorFailedException, NotAbleToCreateTestException {

        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

        AppController appController = new AppController(file);

        boolean activ = true;
        String optiune = null;

        String enunt;
        String varianta1;
        String varianta2;
        String varianta3;
        String variantaCorecta;
        String domeniu;


            while (activ) {
                try {
                System.out.println("");
                System.out.println("1.Adauga intrebare");
                System.out.println("2.Creeaza test");
                System.out.println("3.Statistica");
                System.out.println("4.Exit");
                System.out.println("");

                optiune = console.readLine();

                while (!optiune.equals("4")) {
                    if (optiune.equals("1")) {
                        System.out.println("Enunt : ");
                        enunt = console.readLine();
                        System.out.println("Varianta1 : ");
                        varianta1 = console.readLine();
                        System.out.println("Varianta2 : ");
                        varianta2 = console.readLine();
                        System.out.println("Varianta3 : ");
                        varianta3 = console.readLine();
                        System.out.println("Varianta corecta : ");
                        variantaCorecta = console.readLine();
                        System.out.println("Domeniu :");
                        domeniu = console.readLine();
                        appController.addNewIntrebare(enunt, varianta1, varianta2,varianta3, variantaCorecta, domeniu);

                    } else if (optiune.equals("2")) {
                        Test newTest = appController.createNewTest();
                        System.out.println("Testul dvs este : ");
                        List<Intrebare> listIntrebariTest = newTest.getIntrebari();
                        for (Intrebare intrebarePrint : listIntrebariTest) {
                            System.out.println(intrebarePrint.toString());
                        }

                    } else if (optiune.equals("3")) {
                        Statistica statistica;
                        try {
                            statistica = appController.getStatistica();
                            System.out.println(statistica);
                        } catch (NotAbleToCreateStatisticsException e) {
                            // TODO
                        }

                    } else if (optiune.equals("4")) {
                        activ = false;
                        break;
                    } else {
                        System.out.println("Comanda incorecta");
                    }

                    optiune = console.readLine();
                }
            }
             catch(IOException e){
                System.out.println(e);
            }
        catch(DuplicateIntrebareException e){
                System.out.println(e);
            }
        catch(IntrebareValidatorFailedException e){
                System.out.println(e);
            }
        catch(NotAbleToCreateTestException e){
                System.out.println(e);
            }
        }



        }

    }
