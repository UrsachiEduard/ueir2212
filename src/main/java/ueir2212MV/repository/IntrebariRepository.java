package ueir2212MV.repository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;


import ueir2212MV.model.Intrebare;
import ueir2212MV.exception.DuplicateIntrebareException;

public class IntrebariRepository {

	private List<Intrebare> intrebari;
	
	public IntrebariRepository() {
		setIntrebari(new LinkedList<Intrebare>());
	}
	public int dim(){
		return intrebari.size();
	}
	public void addIntrebare(Intrebare intrebare) throws DuplicateIntrebareException{
		if(exists(intrebare))
			throw new DuplicateIntrebareException("Intrebarea deja exista!");
		intrebari.add(intrebare);
		System.out.println("Intrebarea a fost adaugata!");
	}
	
	public boolean exists(Intrebare intrebare){
		for(Intrebare intrebareFromIntrebari : intrebari)
			if(intrebareFromIntrebari.equals(intrebare))
				return true;
		return false;
	}
	
	public Intrebare pickRandomIntrebare(){
		Random random = new Random();
		return intrebari.get(random.nextInt(intrebari.size()));
	}
	
	public int getNumberOfDistinctDomains(){
		return getDistinctDomains().size();
		
	}
	
	public Set<String> getDistinctDomains(){
		Set<String> domains = new TreeSet<String>();
		for(Intrebare intrebre : intrebari)
			domains.add(intrebre.getDomeniu());
		return domains;
	}
	
	public List<Intrebare> getIntrebariByDomain(String domain){
		List<Intrebare> intrebariByDomain = new LinkedList<Intrebare>();
		for(Intrebare intrebare : intrebari){
			if(intrebare.getDomeniu().equals(domain)){
				intrebariByDomain.add(intrebare);
			}
		}
		
		return intrebariByDomain;
	}
	
	public int getNumberOfIntrebariByDomain(String domain){
		return getIntrebariByDomain(domain).size();
	}
	
	public List<Intrebare> loadIntrebariFromFile(String file) {

		List<Intrebare> intrebari = new LinkedList<Intrebare>();
		try (BufferedReader in = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = in.readLine()) != null) {
				String fields[] = line.split(";");
				String enunt = fields[0];
				String varianta1 = fields[1];
				String varianta2 = fields[2];
				String varianta3 = fields[3];
				String variantaCorecta = fields[4];
				String domeniu = fields[5];
				Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2,varianta3, variantaCorecta, domeniu);
				intrebari.add(intrebare);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return intrebari;
	}

		public List<Intrebare> getIntrebari() {
		return intrebari;
	}

	public void setIntrebari(List<Intrebare> intrebari) {
		this.intrebari = intrebari;
	}
	
}
