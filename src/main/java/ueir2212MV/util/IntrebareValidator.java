package ueir2212MV.util;


import ueir2212MV.controller.AppController;
import ueir2212MV.exception.IntrebareValidatorFailedException;

public class IntrebareValidator extends AppController {

	public IntrebareValidator(String file) {
		super(file);
	}

	public static void validateEnunt(String enunt) throws IntrebareValidatorFailedException {

		if(enunt.equals(""))
			throw new IntrebareValidatorFailedException("Enuntul este vid!");
		if(!Character.isUpperCase(enunt.charAt(0)))
			throw new IntrebareValidatorFailedException("Prima litera din enunt nu e majuscula!");
		if(!String.valueOf(enunt.charAt(enunt.length()-1)).equals("?"))
			throw new IntrebareValidatorFailedException("Ultimul caracter din enunt nu e '?'!");
		if(enunt.length() > 100)
			throw new IntrebareValidatorFailedException("Lungimea enuntului depaseste 100 de caractere!");
		
	}
	
	public static void validateVarianta1(String varianta1) throws IntrebareValidatorFailedException {
		
		varianta1 = varianta1.trim();
		
		if(varianta1.equals(""))
			throw new IntrebareValidatorFailedException("Varianta1 este vida!");
		if(!String.valueOf(varianta1.charAt(0)).equals("1") || !String.valueOf(varianta1.charAt(1)).equals(")"))
			throw new IntrebareValidatorFailedException("Varianta1 nu incepe cu '1)'!");
		if(varianta1.length() > 50)
			throw new IntrebareValidatorFailedException("Lungimea variantei1 depaseste 50 de caractere!" );
	}
	
	public static void validateVarianta2(String varianta2) throws IntrebareValidatorFailedException {
		
		varianta2 = varianta2.trim();
		
		if(varianta2.equals(""))
			throw new IntrebareValidatorFailedException("Varianta2 este vida!");
		if(!String.valueOf(varianta2.charAt(0)).equals("2") || !String.valueOf(varianta2.charAt(1)).equals(")"))
			throw new IntrebareValidatorFailedException("Varianta2 nu incepe cu '2)'!");
		if(varianta2.length() > 50)
			throw new IntrebareValidatorFailedException("Lungimea variantei2 depaseste 50 de caractere!" );
	}
	
	public static void validateVarianta3(String varianta3) throws IntrebareValidatorFailedException {
		
		varianta3 = varianta3.trim();
		
		if(varianta3.equals(""))
			throw new IntrebareValidatorFailedException("Varianta3 este vida!");
		if(!String.valueOf(varianta3.charAt(0)).equals("3") || !String.valueOf(varianta3.charAt(1)).equals(")"))
			throw new IntrebareValidatorFailedException("Varianta3 nu incepe cu '3)'!");
		if(varianta3.length() > 50)
			throw new IntrebareValidatorFailedException("Lungimea variantei3 depaseste 50 de caractere!" );
	
	}
	
	public static void validateVariantaCorecta(String variantaCorecta) throws IntrebareValidatorFailedException {
		
		variantaCorecta = variantaCorecta.trim();
		
		if(!variantaCorecta.equals("1") && !variantaCorecta.equals("2") && !variantaCorecta.equals("3"))
			throw new IntrebareValidatorFailedException("Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}");
	}
	
	public static void validateDomeniu(String domeniu) throws IntrebareValidatorFailedException {
		
		domeniu = domeniu.trim();
		
		if(domeniu.equals(""))
			throw new IntrebareValidatorFailedException("Domeniul este vid!");
		if(!Character.isUpperCase(domeniu.charAt(0)))
			throw new IntrebareValidatorFailedException("Prima litera din domeniu nu e majuscula!");
		if(domeniu.length() > 30)
			throw new IntrebareValidatorFailedException("Lungimea domeniului depaseste 30 de caractere!");
		if(!(domeniu.equals("Engleza") || domeniu.equals("Informatica") || domeniu.equals("Matematica") || domeniu.equals("Istorie") || domeniu.equals("Sport"))) //am adaugat inca 2 pentru Lab3
			throw new IntrebareValidatorFailedException("Domeniul trebuie sa fie Engleza/Informatica/Matematica/Istorie/Sport");
		
	}
}
