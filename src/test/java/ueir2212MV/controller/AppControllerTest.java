package ueir2212MV.controller;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ueir2212MV.exception.DuplicateIntrebareException;
import ueir2212MV.exception.IntrebareValidatorFailedException;
import ueir2212MV.exception.NotAbleToCreateTestException;
import ueir2212MV.model.Intrebare;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Edi on 4/16/2019.
 */
public class AppControllerTest {
    private AppController appController;

    @Before
    public void init(){
        appController = new AppController("C:\\Users\\Edi\\Desktop\\An 3 Semestrul 2\\VVSS\\ueir2212\\src\\main\\java\\ueir2212MV\\intrebari.txt");
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void tc1F02() throws NotAbleToCreateTestException {
        expectedEx.expect(ueir2212MV.exception.NotAbleToCreateTestException.class);
        expectedEx.expectMessage("Nu exista suficiente intrebari pentru crearea unui test!(5)");
        AppController appController2 = new AppController("C:\\Users\\Edi\\Desktop\\An 3 Semestrul 2\\VVSS\\ueir2212\\src\\main\\java\\ueir2212MV\\intrebari2.txt");

        appController2.createNewTest();


    }
    @Test
    public void tc2F02() throws NotAbleToCreateTestException {
        expectedEx.expect(ueir2212MV.exception.NotAbleToCreateTestException.class);
        expectedEx.expectMessage("Nu exista suficiente domenii pentru crearea unui test!(5)");
        AppController appController2 = new AppController("C:\\Users\\Edi\\Desktop\\An 3 Semestrul 2\\VVSS\\ueir2212\\src\\main\\java\\ueir2212MV\\intrebari3.txt");

        appController2.createNewTest();


    }
    @Test
    public void tc3AndTc4F02() throws NotAbleToCreateTestException {

        AppController appController2 = new AppController("C:\\Users\\Edi\\Desktop\\An 3 Semestrul 2\\VVSS\\ueir2212\\src\\main\\java\\ueir2212MV\\intrebari.txt");

        List<ueir2212MV.model.Test> testIntrebari = new ArrayList<>();
        ueir2212MV.model.Test t = appController2.createNewTest();
        testIntrebari.add(t);
        assertEquals(testIntrebari.size(), 1);


    }
    @Test
    public void testValid() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        String enunt = "Enunt?";
        String varianta1 = "1)varianta1";
        String varianta2 = "2)varianta2";
        String varianta3 = "3)varianta3";
        String variantaCorecta = "2";
        String domeniu = "Engleza";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
        assertEquals(appController.dim(),12);

    }
    @Test
    public void test16() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        String enunt = "Intrebare?";
        String varianta1 = "1)var1";
        String varianta2 = "2)var2";
        String varianta3 = "3)var3";
        String variantaCorecta = "2";
        String domeniu = "Engleza";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);

    }
    @Test
    public void test18() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        String enunt = "Abc...?";
        String varianta1 = "1)var1";
        String varianta2 = "2)var2";
        String varianta3 = "3)var3";
        String variantaCorecta = "2";
        String domeniu = "Engleza";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);

    }

    @Test
    public void test1() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        expectedEx.expect(ueir2212MV.exception.IntrebareValidatorFailedException.class);
        expectedEx.expectMessage("Prima litera din enunt nu e majuscula!");
        String enunt = "lalal";
        String varianta1 = "1)varianta1";
        String varianta2 = "2)varianta2";
        String varianta3 = "3)varianta3";
        String variantaCorecta = "2";
        String domeniu = "Engleza";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
    }
    @Test
    public void test17() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        expectedEx.expect(ueir2212MV.exception.IntrebareValidatorFailedException.class);
        expectedEx.expectMessage("Prima litera din enunt nu e majuscula!");
        String enunt = "intrebare";
        String varianta1 = "1)var1";
        String varianta2 = "2)var2";
        String varianta3 = "3)var3";
        String variantaCorecta = "2";
        String domeniu = "Engleza";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
    }

    @Test
    public void test2() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        expectedEx.expect(ueir2212MV.exception.IntrebareValidatorFailedException.class);
        expectedEx.expectMessage("Lungimea enuntului depaseste 100 de caractere!");
        String enunt = "Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?Enunt?";
        String varianta1 = "1)varianta1";
        String varianta2 = "2)varianta2";
        String varianta3 = "3)varianta3";
        String variantaCorecta = "2";
        String domeniu = "Engleza";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
    }
    @Test
    public void test19() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        expectedEx.expect(ueir2212MV.exception.IntrebareValidatorFailedException.class);
        expectedEx.expectMessage("Lungimea enuntului depaseste 100 de caractere!");
        String enunt = "AbcccccccccccccccccccccccccccccAbcccccccccccccccccccccccccccccccaaaaaaaaaaaaaaaaaaaaaaaaAbcccccccccccccccccccccccccccccccaaaaaaaaaaaaaaaaaaaaaaaaAbcccccccccccccccccccccccccccccccaaaaaaaaaaaaaaaaaaaaaaaaAbcccccccccccccccccccccccccccccccaaaaaaaaaaaaaaaaaaaaaaaaAbcccccccccccccccccccccccccccccccaaaaaaaaaaaaaaaaaaaaaaaaccaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaddddddddddddddddddddddddddddddddddddddddddddssssssssssssssssssssssssssssssssssssssssssssssssssssss?";
        String varianta1 = "1)varianta1";
        String varianta2 = "2)varianta2";
        String varianta3 = "3)varianta3";
        String variantaCorecta = "2";
        String domeniu = "Engleza";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
    }

    @Test
    public void test3() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        expectedEx.expect(ueir2212MV.exception.IntrebareValidatorFailedException.class);
        expectedEx.expectMessage("Domeniul trebuie sa fie Engleza/Informatica/Matematica");
        String enunt = "Enunt corect?";
        String varianta1 = "1)varianta1";
        String varianta2 = "2)varianta2";
        String varianta3 = "3)varianta3";
        String variantaCorecta = "1";
        String domeniu = "DomeniuGreusit";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
    }

    @Test
    public void test4() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        expectedEx.expect(ueir2212MV.exception.IntrebareValidatorFailedException.class);
        expectedEx.expectMessage("Lungimea variantei2 depaseste 50 de caractere!" );
        String enunt = "Enunt?";
        String varianta1 = "1)varianta1";
        String varianta2 = "2)Lungimea variantei2 depaseste 50 de caractere!Lungimea variantei2 depaseste 50 de caractere!Lungimea variantei2 depaseste 50 de caractere!Lungimea variantei2 depaseste 50 de caractere!Lungimea variantei2 depaseste 50 de caractere!Lungimea variantei2 depaseste 50 de caractere!Lungimea variantei2 depaseste 50 de caractere!Lungimea variantei2 depaseste 50 de caractere!Lungimea variantei2 depaseste 50 de caractere!Lungimea variantei2 depaseste 50 de caractere!Lungimea variantei2 depaseste 50 de caractere!";
        String varianta3 = "3)varianta3";
        String variantaCorecta = "2";
        String domeniu = "Engleza";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
    }

    @Test
    public void test5() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        expectedEx.expect(ueir2212MV.exception.IntrebareValidatorFailedException.class);
        expectedEx.expectMessage("Enuntul este vid!");
        String enunt = "";
        String varianta1 = "1)varianta1";
        String varianta2 = "2)varianta2";
        String varianta3 = "3)varianta3";
        String variantaCorecta = "1";
        String domeniu = "Engleza";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
    }
    @Test
    public void test14() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        expectedEx.expect(ueir2212MV.exception.IntrebareValidatorFailedException.class);
        expectedEx.expectMessage("Enuntul este vid!");
        String enunt = "";
        String varianta1 = "1)var1";
        String varianta2 = "2)var2";
        String varianta3 = "3)var3";
        String variantaCorecta = "1";
        String domeniu = "Engleza";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
    }

    @Test
    public void test6() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        expectedEx.expect(ueir2212MV.exception.IntrebareValidatorFailedException.class);
        expectedEx.expectMessage("Varianta2 nu incepe cu '2)'!");
        String enunt = "Enunt?";
        String varianta1 = "1)varianta1";
        String varianta2 = "varianta2";
        String varianta3 = "3)varianta3";
        String variantaCorecta = "2";
        String domeniu = "Domeniuldomeniiilor";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
    }
    @Test
    public void test7() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        expectedEx.expect(ueir2212MV.exception.IntrebareValidatorFailedException.class);
        expectedEx.expectMessage("Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}");
        String enunt = "Enunt?";
        String varianta1 = "1)varianta1";
        String varianta2 = "2)varianta2";
        String varianta3 = "3)varianta3";
        String variantaCorecta = "varianta gresita";
        String domeniu = "Domeniuldomeniiilor";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);

    }
    @Test
    public void test23() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        expectedEx.expect(ueir2212MV.exception.IntrebareValidatorFailedException.class);
        expectedEx.expectMessage("Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}");
        String enunt = "Intrebare?";
        String varianta1 = "1)var1";
        String varianta2 = "2)var2";
        String varianta3 = "3)var3";
        String variantaCorecta = "14";
        String domeniu = "Informatica";

        appController.addNewIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);

    }
}