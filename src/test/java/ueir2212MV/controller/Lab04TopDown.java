package ueir2212MV.controller;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ueir2212MV.exception.DuplicateIntrebareException;
import ueir2212MV.exception.IntrebareValidatorFailedException;
import ueir2212MV.exception.NotAbleToCreateStatisticsException;
import ueir2212MV.exception.NotAbleToCreateTestException;
import ueir2212MV.model.Statistica;

/**
 * Created by Edi on 5/19/2019.
 */
public class Lab04TopDown {
    private AppController appController;

    @Before
    public void setUp() throws Exception {
        appController = new AppController();
    }
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testA() throws DuplicateIntrebareException, IntrebareValidatorFailedException {
        //A valid
        //Intrebare i1 = new Intrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Sport");
        //Intrebare i2 = new Intrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Matematica");
        //Intrebare i3 = new Intrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Informatica");
        //Intrebare i4 = new Intrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Istorie");
        //Intrebare i5 = new Intrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Engleza");

        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Sport");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Matematica");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Informatica");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Istorie");
        //appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Engleza");

        assert(appController.dim() == 4);

    }

    @Test
    public void testB() throws NotAbleToCreateTestException, DuplicateIntrebareException, IntrebareValidatorFailedException {
        //P->B->A  A-valid   B-invalid
        expectedEx.expect(ueir2212MV.exception.NotAbleToCreateTestException.class);
        expectedEx.expectMessage("Nu exista suficiente intrebari pentru crearea unui test!(5)");

        appController.createNewTest();

    }

    @Test
    public void testC() throws NotAbleToCreateStatisticsException {
        //testare de integrare a modulului C;
        //c invalid
        expectedEx.expect(ueir2212MV.exception.NotAbleToCreateStatisticsException.class);
        expectedEx.expectMessage("Repository-ul nu contine nicio intrebare!");

        Statistica s = appController.getStatistica();
        System.out.println(s.getIntrebariDomenii().size());
        //assert(s.getIntrebariDomenii().size() == 5);
    }

    @Test
    public void testCombinat1() throws DuplicateIntrebareException, IntrebareValidatorFailedException, NotAbleToCreateStatisticsException {
        //P->A
        //a
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Sport");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Matematica");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Informatica");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Istorie");
        //appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Engleza");

        assert(appController.dim() == 4);

    }
    @Test
    public void testCombinat2() throws DuplicateIntrebareException, IntrebareValidatorFailedException, NotAbleToCreateStatisticsException, NotAbleToCreateTestException {
        //P->A->B
        //a
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Sport");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Matematica");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Informatica");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Istorie");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Engleza");

        assert(appController.dim() == 5);

        //b
        ueir2212MV.model.Test t = appController.createNewTest();
        assert(t.getIntrebari().size() == 5);
    }

    @Test
    public void testCombinat3() throws DuplicateIntrebareException, IntrebareValidatorFailedException, NotAbleToCreateTestException, NotAbleToCreateStatisticsException {
        //P->A->B->C
        //a
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Sport");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Matematica");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Informatica");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Istorie");
        appController.addNewIntrebare("Enunt?","1)rasp1","2)rasp2","3)rasp3","2","Engleza");

        assert(appController.dim() == 5);

        //b
        ueir2212MV.model.Test t = appController.createNewTest();
        assert(t.getIntrebari().size() == 5);
        //c

        Statistica s = appController.getStatistica();

        assert(s.getIntrebariDomenii().size() == 5);
    }

}
